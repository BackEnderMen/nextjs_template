import React from 'react'
import type { AppProps } from 'next/app'
import '@/styles/globals.scss'
import { Provider } from 'react-redux'
import { store } from '@/store'
import { Layout } from '@/containers/Layout/Layout'

export default function App({ Component, pageProps }: AppProps) {
  return (
    <Provider store={store}>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </Provider>
  )
}
