import { configureStore, combineReducers } from '@reduxjs/toolkit'
import { createWrapper } from 'next-redux-wrapper'

import globalReducer from './global/global.slice'

const rootReducer = combineReducers({
  global: globalReducer,
})

export const store = configureStore({
  reducer: rootReducer,
})

const makeStore = () => store

export type RootState = ReturnType<typeof store.getState>

export const wrapper = createWrapper(makeStore)
