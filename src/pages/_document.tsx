import { Html, Head, Main, NextScript } from 'next/document'

export default function Document() {
  return (
    <Html lang='en'>
      <Head>
        <link
          rel='stylesheet'
          href='https://fonts.googleapis.com/css?family=Oswald:100,200,300,400,500,600,700,800,900|Lato:100,200,300,400,500,600,700,800,900|Futura:100,200,300,400,500,600,700,800,900'
        />
      </Head>
      <body>
        <Main />
        <NextScript />
      </body>
    </Html>
  )
}
