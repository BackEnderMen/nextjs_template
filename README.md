# Next.js Project with TS, Ant Design, SCSS.

This repository contains a Next.js project that is set up with Ant Design for styling, SCSS for custom styles, Prettier for code formatting, and ESLint for code linting. This combination provides a solid foundation for building modern web applications with a consistent coding style and best practices.

## Table of Contents

- [Getting Started](#getting-started)
    - [Prerequisites](#prerequisites)
    - [Installation](#installation)
- [Usage](#usage)
- [Folder Structure](#folder-structure)
- [Styling](#styling)
- [Code Formatting](#code-formatting)
- [Linting](#linting)
- [Contributing](#contributing)
- [License](#license)

## Getting Started

### Prerequisites

Before you begin, ensure you have the following installed on your system:

- [Node.js](https://nodejs.org/) (version >= 16.x) (i use v16.14.0)
- [npm](https://www.npmjs.com/) (typically comes with Node.js installation)

### Installation

1. Clone this repository:

   ```bash
   git clone https://gitlab.com/BackEnderMen/nextjs_template.git
   ```

2. Navigate to the project directory:

   ```bash
   cd nextjs_template
   ```

3. Install dependencies:

   ```bash
   npm install
   ```

## Usage

Start the development server:

```bash
npm run dev
```

Visit `http://localhost:3000` in your web browser to see the application.

## Folder Structure

The project follows a standard folder structure commonly used in Next.js applications:

- `public`: Houses static assets that are directly served by Next.js.
- `api`: Contains server-side functionality and API handlers.
- `components`: Contains reusable UI components.
- `constants`: Stores application-wide constants.
- `containers`: Holds logical components that connect UI components. Basically not reusable.
- `hooks`: Includes reusable custom react hooks for functional logic.
- `pages`: Contains the main pages/routing of the application.
- `styles`: Holds global styles and SCSS files.
- `utils`: Houses utility functions and helpers.

Feel free to adjust and expand this structure as your project grows.

## Styling

This project utilizes [Ant Design](https://ant.design/) for pre-built UI components and [SCSS](https://sass-lang.com/) for custom styling. You can customize the styling by modifying the SCSS files in the `styles` directory.

## Code Formatting

[Prettier](https://prettier.io/) is set up to automatically format your code for consistent and clean formatting. The configuration can be found in the `.prettierrc` file.

## Linting

[ESLint](https://eslint.org/) is configured to maintain code quality and adherence to coding standards. The ESLint configuration can be found in the `.eslintrc.json` file.

## Containers

The `containers` directory is dedicated to holding logical components that connect UI components. This separation of concerns helps in maintaining a clear structure for your application's architecture.

## Hooks

The `hooks` directory contains reusable custom hooks that encapsulate functional logic. This promotes code reuse and a modular approach to building your application.

## API Folder

The `api` directory contains server-side functionality and API handlers. You can create API routes in this folder to handle various server-side tasks.

## Utils

The `utils` directory holds utility functions and helper modules that can be reused across your application.

## Constants

The `constants` directory stores application-wide constants that can be easily accessed from various parts of your project.

## Build

To build your application for production deployment, run:

```bash
npm run build
```

This command will generate an optimized production build in the `./next` directory.

## Contributing

Contributions are welcome! If you find any issues or want to enhance the project, feel free to create a pull request.

## License

This project is licensed under the [MIT License](LICENSE).

---

Happy coding!

Viktor
