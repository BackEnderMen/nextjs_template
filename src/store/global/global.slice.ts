import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { IStore } from '../types'

const initialState: IStore['global'] = {
  showMainMenu: false,
}

export const defSlice = createSlice({
  name: 'global',
  initialState,
  reducers: {
    setGlobal: (state: IStore['global'], action: PayloadAction<IStore['global']>) => ({ ...state, ...action?.payload }),
  },
})

export const setGlobal: (payload: IStore['global']) => PayloadAction<IStore['global']> = defSlice.actions.setGlobal

export default defSlice.reducer
