import { store } from './index'
import { IGlobal } from '@/store/global/global.types'

interface Root<T> {
  loading: boolean
  errors: any
  data: T | null
}

export interface IStore {
  global: IGlobal
}

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
