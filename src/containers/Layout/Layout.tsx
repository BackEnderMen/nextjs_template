import React, { useEffect } from 'react'
import styles from './Layout.module.scss'
import { useDispatch } from 'react-redux'
import { setGlobal } from '@/store/global/global.slice'

interface Props {
  children: React.ReactNode
}

export const Layout: React.FC<Props> = ({ children }) => {
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(setGlobal({ showMainMenu: true }))
  }, [])

  return (
    <div className={styles.mainContainer}>
      <main className={styles.container}>{children}</main>
    </div>
  )
}
